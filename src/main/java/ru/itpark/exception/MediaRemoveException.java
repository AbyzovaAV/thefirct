package ru.itpark.exception;

public class MediaRemoveException extends RuntimeException {
    public MediaRemoveException() {
    }

    public MediaRemoveException(String message) {
        super(message);
    }

    public MediaRemoveException(String message, Throwable cause) {
        super(message, cause);
    }

    public MediaRemoveException(Throwable cause) {
        super(cause);
    }

    public MediaRemoveException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
