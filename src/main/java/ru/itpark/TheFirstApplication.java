package ru.itpark;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.entity.AccountEntity;
import ru.itpark.entity.PostEntity;
import ru.itpark.repositort.AccountRepository;
import ru.itpark.repositort.PostRepository;

import java.util.List;

@SpringBootApplication
public class TheFirstApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(TheFirstApplication.class, args);

        context
                .getBean(PostRepository.class)
                .saveAll(List.of(
                        new PostEntity(
                                0,
                                "Часть первая. Работники. Роберт Кийоскаи.",
                                "Р - Рабочие (работники по найму). Это люди, которые трудятся за зарплату. Например, менеджер в офисе (офисный планктон)," +
                                        "работник на предприятии, секретарь, дворник, токарь и другие профессии. Объединяет их то, что все они продают своё время " +
                                        "работодателю. Как вы понимаете, люди из категории Р - это большинство. Плюсы здесь - это стабильность, гарантии, всю ответственность " +
                                        "несёт работодатель, стабильный доход.",
                                "workpeople.png"),
                        new PostEntity(
                                0,
                                "Часть вторная. Работующие на себя. Роберт Кийосаки.",
                                " С - работающий на себя. Это может быть финансовый консультант, частный юрист, стоматолог, частный предприниматель. От первой " +
                                        "категории  эти люди отличаются тем, что уже не работают «на дядю», поэтому отсюда вытекают такие следствия:",
                                "workpeople.png"),
                        new PostEntity(
                                0,
                                "Часть третья. Владельцы бизнеса. Роберт Кийосаки.",
                                "Б - владельцы бизнеса. Б относится уже к правой стороне квадранта денежного потока, поэтому здесь дела отстоят по другому. Здесь критерием" +
                                        "выступает вот какой момент. Владельцем бизнеса можно назвать того, кто может запросто уехать в отпуск, кругосветное путешествие, в общем" +
                                        "отойти от дел, от управления бизнесов. Если к моменту его возвращения бизнес не развалится и станет ещё прибыльнее, то этот человек - представитель категории Б.",
                                "biz.png"),
                        new PostEntity(
                                0,
                                "Часть четвертая. Инвесторы. Роберт Кийосакию",
                                "И - инвесторы. Это люди, которые получают деньги от инвестиций. Они вкладывают средства в различные инвестиционные инструменты и получают с этого доход." +
                                        "Это как раз тот случай, когда уже не вы работаете на деньги - ваши деньги работают на вас",
                                "ivestor.png")
                ));

        var encoder = context.getBean(PasswordEncoder.class);
        context
                .getBean(AccountRepository.class)
                .saveAll(List.of(
                        new AccountEntity(
                                0,
                                "nastya",
                                encoder.encode("nastya"),
                                List.of(new SimpleGrantedAuthority("ADMIN")),
                                true,
                                true,
                                true,
                                true),
                        new AccountEntity(
                                0,
                                "gleb",
                                encoder.encode("gleb"),
                                List.of(new SimpleGrantedAuthority("ADMIN")),
                                true,
                                true,
                                true,
                                true)
                ));




    }
}
