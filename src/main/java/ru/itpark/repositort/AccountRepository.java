package ru.itpark.repositort;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itpark.entity.AccountEntity;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {
    Optional<AccountEntity> findByUsername(String username);
}
