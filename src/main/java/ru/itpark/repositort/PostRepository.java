package ru.itpark.repositort;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.PostEntity;

import java.util.List;

public interface PostRepository extends JpaRepository<PostEntity, Integer> {

    @Query("SELECT e FROM PostEntity e WHERE LOWER(e.name) LIKE LOWER(:name)")
    List<PostEntity> findByName(@Param("name") String name);
}
