package ru.itpark.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;
import ru.itpark.dto.PostDTO;
import ru.itpark.entity.PostEntity;
import ru.itpark.exception.MediaRemoveException;
import ru.itpark.exception.MediaUploadException;
import ru.itpark.exception.PostNotFoundException;
import ru.itpark.repositort.PostRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final Path mediaPath;

    public PostServiceImpl(PostRepository postRepository,
                           @Value("C:\\javaItPark\\TheFirst\\files\\media")
                           String mediaPath) {
        this.postRepository = postRepository;
        this.mediaPath = Paths.get(mediaPath);
    }


    public List<PostEntity> findAll() {
        return postRepository.findAll();
    }

    @Override
    public List<PostEntity> findByName(String name) {
        return postRepository.findByName("%" + name + "%");
    }

    @Override
    public PostEntity findById(int id) {
        return postRepository.findById(id).orElseThrow(PostNotFoundException::new);
    }

    @Override
    public void deleteById(int id) {
        var entity = postRepository.findById(id).orElseThrow(PostNotFoundException::new);
        Path target = mediaPath.resolve(entity.getImg());

        try {
            Files.deleteIfExists(target);
        } catch (IOException e) {
            throw new MediaRemoveException(e);
        }
        postRepository.delete(entity);
    }

    @Override
    public void save(PostDTO dto) {
        var contentTypeImg = dto.getImg().getContentType();

        String name = UUID.randomUUID().toString();
        String ext;

        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeImg)){
            ext = ".jpg";
        }else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeImg)){
            ext= "pig";
        }else {
            throw new UnsupportedMediaTypeStatusException(contentTypeImg);
        }

        Path target = mediaPath.resolve(name + ext);

        try {
            dto.getImg().transferTo(target.toFile());
        } catch (IOException e) {
            throw new MediaUploadException(e);
        }
        PostEntity entity = new PostEntity(0, dto.getName(), dto.getDescription(), name + ext);
        postRepository.save(entity);

    }

    @Override
    public void redactById(int id, PostDTO dto) {
        //TODO
        PostEntity initialEntity = postRepository.findById(id).orElseThrow(PostNotFoundException::new);

        var contentTypeIMG = dto.getImg().getContentType();

        String name;
        String ext;
        Path target;

        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeIMG)) {
            name =UUID.randomUUID().toString();
            ext = ".jpg";
            target = mediaPath.resolve(name + ext);
            try {
                dto.getImg().transferTo(target.toFile());
            } catch (IOException e) {
                throw new MediaUploadException(e);
            }
            PostEntity entity = new PostEntity(id, dto.getName(), dto.getDescription(), name + ext);
            postRepository.save(entity);
        } else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeIMG)){
            name = UUID.randomUUID().toString();
            ext = ".png";
            target = mediaPath.resolve(name + ext);
            try {
                dto.getImg().transferTo(target.toFile());
            } catch (IOException e) {
                throw new MediaUploadException(e);
            }
            PostEntity entity = new PostEntity(id, dto.getName(), dto.getDescription(), name + ext);
            postRepository.save(entity);
        }else if (dto.getImg().isEmpty()){
            PostEntity entity = new PostEntity(id, dto.getName(), dto.getDescription(), initialEntity.getImg());
            postRepository.save(entity);
        }
        else {
            throw new UnsupportedMediaTypeStatusException(contentTypeIMG);
        }

    }
}
