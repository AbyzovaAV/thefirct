package ru.itpark.service;

import ru.itpark.dto.PostDTO;
import ru.itpark.entity.PostEntity;

import java.util.List;

public interface PostService {
    List<PostEntity> findAll();

    List<PostEntity> findByName(String name);

    PostEntity findById(int id);

    void deleteById(int id);

    void save(PostDTO dto);

    void redactById(int id, PostDTO dto);
}
