package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.dto.PostDTO;
import ru.itpark.service.PostService;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final PostService postService;

    public AdminController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("items", postService.findAll());
        return "admin/adminpanel";
    }

    @PostMapping("/{id}/delete")
    public String deleteById(@PathVariable int id){
        postService.deleteById(id);
        return "redirect:/admin";
    }

    @PostMapping
    public String save(@Valid @ModelAttribute PostDTO dto) {
        postService.save(dto);
        return "redirect:/admin";
    }

    @RequestMapping("/search")
    public String findByName(
            @RequestParam String name, Model model
    ){
        model.addAttribute("search", name);
        model.addAttribute("items", postService.findByName(name));
        return "admin/adminpanel";
    }

    @PostMapping("/{id}/redact")
    public String redact(@PathVariable int id, @Valid @ModelAttribute PostDTO dto){
        postService.redactById(id, dto);
        return "redirect:/admin";
    }

    @GetMapping("/{id}/redact")
    public String getRedact(
            @PathVariable int id, Model model
    ){
        model.addAttribute("item", postService.findById(id));
        return "admin/redact";
    }
}
