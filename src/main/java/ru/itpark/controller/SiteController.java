package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itpark.service.PostService;

@Controller
@RequestMapping("/")
public class SiteController {
    private final PostService postService;

    public SiteController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("items", postService.findAll());
        return "posts";
    }

    @RequestMapping("/search")
    public String findByName(
            @RequestParam String name, Model model
    ){
        model.addAttribute("search", name);
        model.addAttribute("items", postService.findByName(name));
        return "posts";
    }

    @GetMapping("/{id:\\d+}")
    public String get(
            @PathVariable int id, Model model){
        model.addAttribute("item", postService.findById(id));
        return "post";
    }
}
