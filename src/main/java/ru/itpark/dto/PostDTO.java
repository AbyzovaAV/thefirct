package ru.itpark.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {
    @NotNull
    @Size(min = 5, max = 100)
    private String name;
    @NotNull
    private String description;
    @NotNull
    private MultipartFile img;
}
